# Logistic regression

This classifier predicts whether the wine is white (class label 0) or red (class label 1). 

The wine dataset given in data.mat consists of 6,497 data points, each having 12 features. The description of these features is provided in data.mat. 