# Gaussian discriminant analysis.

The input data is the MNIST dataset, and we simply have to assign it a label accurately using either Linear Discriminant Analysis (LDA) or Quadratic Discriminant Analysis (QDA).