# Convolutional Neural Networks for Image Classification

This is the most complex project. Here I developed several convolutional neural network models for image classification.
    
The training dataset (called MDS189) was made by UC Berkeley students who took videos of themselves performing basic exercises such as squats, push-ups and others. Each image was taken by a student in various conditions, different backgrounds, lightings and so on. For privacy reasons the input data that was used for this project is not revealed. 
    
I used pytorch to create convolutional neural networks that classify these images. Furthermore, I created a list of potential CNN models (See file "List_of_CNNs_for_Training.py" ), and the script ( "Training_Script.ipynb" ) loads these models and runs them in the GPU. The results are then saved in the folder "Results", where I keep track of the CNN model used, the transforms made to the input images, and a graph showing the performance of each of those models. 
    
Many toy datasets in machine learning (and computer vision) serve as excellent tools to develop intuitions about methods, but they cannot be directly used in real-world problems. Project 4 could be.