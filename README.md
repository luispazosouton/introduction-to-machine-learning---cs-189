# Introduction to Machine Learning - CS 189

This repository shows some of the code that I wrote for the course on Introduction to Machine Learning  (CS 189) from UC Berkeley. I organized it in 4 projects of increasing complexity.

- Project 1:

    A simple gaussian classifier, using Linear Discriminant Analysis (LDA) and Quadratic Discriminant Analysis (QDA).

- Project 2:

    Very simple logistic regression algorithm to classify whether wine is white or red.

- Project 3:

    In this problem, I implemented decision trees and random forests for classification on two datasets:
 
    1. The spam dataset.
    2. Titanic dataset to predict Titanic survivors. 
        
- Project 4:

    This is the most complex project. Here I developed several convolutional neural network models for image classification.
    
    The training dataset (called MDS189) was made by UC Berkeley students who took videos of themselves performing basic exercises such as squats, push-ups and others. Each image was taken by a student in various conditions, different backgrounds, lightings and so on. For privacy reasons the input data that was used for this project is not revealed. 
    
    I used pytorch to create convolutional neural networks that classify these images. Furthermore, I created a list of potential CNN models (See file "List_of_CNNs_for_Training.py" ), and the script ( "Training_Script.ipynb" ) loads these models and runs them in the GPU. The results are then saved in the folder "Results", where I keep track of the CNN model used, the transforms made to the input images, and a graph showing the performance of each of those models. 
    
Many toy datasets in machine learning (and computer vision) serve as excellent tools to develop intuitions about methods, but they cannot be directly used in real-world problems. Project 4 could be.
