# Decision Trees

In this problem, I implemented decision trees and random forests for classification on two datasets:

    1) The spam dataset.
    2) Titanic dataset to predict Titanic survivors. 